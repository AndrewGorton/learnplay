SBT_OPTS="-Xms512M -Xmx1536M -Xss1M -XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=256M -Dspecs2.color=true -Dsbt.log.format=true"
java $SBT_OPTS -jar `dirname $0`/sbt-launch.jar "$@"
