package controllers

import controllers.Application._
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc.Action

case class UserData(email: String, password: String)

object Auth {
  val userDataForm = Form(
    mapping(
      "email" -> nonEmptyText,
      "password" -> nonEmptyText
    )(UserData.apply)(UserData.unapply)
  )

  def login = Action {
    Ok(views.html.login(userDataForm))
  }
  
  def doLogin = Action { implicit request =>
    userDataForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.login(formWithErrors))
      },
      userData => {
        Redirect(routes.Application.index).withSession(request.session +
          ("loginStatus" -> "loggedIn") + ("user" -> userData.email))
      }
    )
  }
}