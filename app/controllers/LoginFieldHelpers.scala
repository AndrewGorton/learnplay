package controllers

object LoginFieldHelpers {
  import views.html.helper.FieldConstructor
  implicit val myFields = FieldConstructor(views.html.helpers.loginfieldconstructor.f)
}
