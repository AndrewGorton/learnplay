name := "LearnPlay"

version := "3.0.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) =>
{
  case "play/core/server/ServerWithStop.class" => MergeStrategy.first
  case "logback.xml"     => MergeStrategy.first
  case x => old(x)
}
}

libraryDependencies ++= Seq(
  "org.webjars" %% "webjars-play" % "2.3.0-2",
  "org.webjars" % "bootstrap" % "3.3.2-2",
  "commons-codec" % "commons-codec" % "1.10"
)
