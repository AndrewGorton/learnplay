# LearnPlay

A skeleton Scala Play application.

## Building / running

```bash
./sbt ~run
```

then visit http://localhost:9000 to see it in action.
